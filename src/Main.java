import java.util.ArrayList;

public class Main {
    private static String id;
    private static String password;
    private static ArrayList<String> ids = new ArrayList<>();
    private static ArrayList<String> currentUser = new ArrayList<>();
    private static boolean checkGame = false;

    public static void setCheckGame(boolean checkGame) {
        Main.checkGame = checkGame;
    }

    public static boolean isCheckGame() {
        return checkGame;
    }

    public static ArrayList<String> getCurrentUser() {
        return currentUser;
    }

    public static void setCurrentUser(ArrayList<String> currentUser) {
        Main.currentUser = currentUser;
    }

    public static String getPassword() {
        return password;
    }

    public static ArrayList<String> getIds() {
        return ids;
    }

    public static String getId(){
        return id;
    }

    public static void main(String[] args){
        //get ids information from file
        IDcheck.fillList(ids);
        //frame related to id and login part
        MyFrame.IDframe();

        //ArrayList<String> currentUser stores current user's information including ID, password, and chips.

        /*
            menu frame
            cannot be simplified as MyFrame.Menuframe() !!!
         */
        MyFrame menuFrame = new MyFrame();
        menuFrame.Menuframe();//ignore simplify suggestion

        //end the program
        //System.exit(0);


    }
}
