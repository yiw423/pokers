import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

public class MyFrame extends JFrame implements MouseListener {
    private static String id = Main.getId();
    private static ArrayList<String> ids = Main.getIds();
    private static String password = Main.getPassword();
    private static JFrame jf = new JFrame();

    public MyFrame(){
        jf.setTitle("Pokers");
        int fWidth = 1000;
        int fheight = 666;
        jf.setSize(fWidth,fheight);
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        int width = Toolkit.getDefaultToolkit().getScreenSize().width;
        int height = Toolkit.getDefaultToolkit().getScreenSize().height;
        jf.setLocation((width-fWidth)/2,(height-fheight)/2);

        //add bg image & button
        Menuframe();

        jf.setVisible(true);
    }

    public static void IDframe(){
        id = JOptionPane.showInputDialog("ID:");
        //check id
        int idIndex = IDcheck.search(ids,id);
        /*
        check password
        JPasswordfield
         */
        password = JOptionPane.showInputDialog("Password:");
        boolean passwordCheck = IDcheck.passwordCheck(ids,idIndex,password);
        if(idIndex != -1 && passwordCheck){
            /*
            game starts
             */
            //get current user information from ids arraylist
            ArrayList<String> temp = new ArrayList<>();
            temp.add(ids.get(idIndex));
            temp.add(ids.get(idIndex+1));
            temp.add(ids.get(idIndex+2));
            Main.setCurrentUser(temp);

            JOptionPane.showMessageDialog(jf,"Game starts");
        }else if(idIndex != -1){
            JOptionPane.showMessageDialog(jf,"Wrong password.");
            IDframe();
        }else{
            int response = JOptionPane.showConfirmDialog(jf,"ID not exist." +
                    "\nWould u like to set a new id?");
            if(response == 0) {//store id and password as a new id
                IDcheck.insertID(ids, id, password);
                JOptionPane.showMessageDialog(jf, "ID created!");
            }
            IDframe();
        }
    }

    public void Menuframe(){
        /*
            button: one player/multi players/online
            picture: JLabel
         */

        //button
        String OPB_path = "PNG/OPButton.png";
        Icon OPB_icon = new ImageIcon(OPB_path);
        JLabel OPB = new JLabel(OPB_icon);
        //OPB.setBounds(550,360,120,90);
        OPB.setBounds(700,360,120,90);
        jf.add(OPB);
        /*
        String MPB_path = "PNG/MPButton.png";
        Icon MPB_icon = new ImageIcon(MPB_path);
        JLabel MPB = new JLabel(MPB_icon);
        MPB.setBounds(700,360,120,90);
        jf.add(MPB);
        String OLB_path = "PNG/OLButton.png";
        Icon OLB_icon = new ImageIcon(OLB_path);
        JLabel OLB = new JLabel(OLB_icon);
        OLB.setBounds(850,360,120,90);
        jf.add(OLB);

         */

        //MouseListener
        jf.addMouseListener(this);


        //title
        String tt_path = "PNG/title.png";
        Icon tt_icon = new ImageIcon(tt_path);
        JLabel tt = new JLabel(tt_icon);
        tt.setBounds(550,30,427,311);
        jf.add(tt);

        //background image
        String bg_path = "PNG/bg.jpg";
        Icon bg_icon = new ImageIcon(bg_path);
        JLabel bg = new JLabel("",bg_icon,JLabel.CENTER);
        bg.setBounds(0,0,1080,720);
        jf.add(bg);
    }

    @Override
    //for buttons
    public void mouseClicked(MouseEvent e) {
        /*
        all click event print twice
         */
        //One player
        //if(e.getX() > 550 && e.getX() < 670 && e.getY() > 360 && e.getY() < 450){
        if(e.getX() > 700 && e.getX() < 820 && e.getY() > 360 && e.getY() < 450){
            //System.out.println("One player"+buttonReturn);
            new rule();
            jf.setVisible(false);
            //System.exit(0);
        }
        /*
        //Multi players
        if(e.getX() > 700 && e.getX() < 820 && e.getY() > 360 && e.getY() < 450){
            //System.out.println("Multi players"+buttonReturn);
            jf.setVisible(false);
            //System.exit(0);
        }
        //Online
        if(e.getX() > 850 && e.getX() < 970 && e.getY() > 360 && e.getY() < 450){
            //System.out.println("Online"+buttonReturn);
        }

         */
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
