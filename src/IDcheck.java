import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class IDcheck {
    public static void fillList(ArrayList<String> ids){
        try{
            Scanner inputStream = new Scanner(new FileInputStream("ID.txt"));
            while(inputStream.hasNextLine()){
                ids.add(inputStream.nextLine());
            }
            inputStream.close();
        }catch (FileNotFoundException e) {
            System.out.println("File ID.txt was not found.");
            System.exit(0);
        }

    }

    public static int search(ArrayList<String> IDs, String id) throws NullPointerException{
        try{
            for(int i = 0; i < IDs.size(); i+=3){
                if(id.equals(IDs.get(i))){
                    return i;
                }
            }
        }catch(NullPointerException e){
            //when user press cancel at the beginning(a NullPointerException should appear), exit the program
            System.exit(0);
        }
        return -1;
    }

    public static boolean passwordCheck(ArrayList<String> IDs, int IDindex, String password){
        return password.equals(IDs.get(IDindex + 1));
    }

    public static void insertID(ArrayList<String> IDs, String id, String password){
        try{
            PrintWriter outputStream = new PrintWriter(new FileOutputStream("ID.txt"));
            //keep original ids' information in file
            for (String s : IDs) {
                outputStream.println(s);
            }
            //add new id information to file
            outputStream.println(id);
            outputStream.println(password);
            outputStream.println("100000");
            outputStream.close();
            //add new id information to ArrayList
            IDs.add(id);
            IDs.add(password);
            IDs.add("100000");
        }catch (FileNotFoundException e) {
            System.out.println("FileNotFound");
        }
    }
}
