import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class rule extends JFrame {
    private JFrame jf = new JFrame();
    private ArrayList<String> files_path = new ArrayList<>();
    private ArrayList<Icon> cards_icon = new ArrayList<>();

    public rule(){
        jf.setTitle("Pokers");
        int fWidth = 1000;
        int fheight = 666;
        jf.setSize(fWidth,fheight);
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        int width = Toolkit.getDefaultToolkit().getScreenSize().width;
        int height = Toolkit.getDefaultToolkit().getScreenSize().height;
        jf.setLocation((width-fWidth)/2,(height-fheight)/2);

        bg();//add background image

        jf.setVisible(true);
    }

    public void bg(){
        //background image
        String desk_path = "PNG/desk.png";
        Icon desk_icon = new ImageIcon(desk_path);
        JLabel desk = new JLabel("",desk_icon,JLabel.CENTER);
        desk.setBounds(0,0,1000,666);
        jf.add(desk);
    }

    public void getFiles() {
        File file = new File("PNG/cards");
        File[] tempList = file.listFiles();

        assert tempList != null;
        for(int i = 0; i < tempList.length; i++){
            if (tempList[i].isFile()) {
                //System.out.println("文件：" + tempList[i]);
                files_path.add(tempList[i].toString());
            }
            /*
            if (tempList[i].isDirectory()) {
                System.out.println("文件夹：" + tempList[i]);
            }
             */
        }

        //delete .DS_Store
        files_path.remove("PNG/cards/.DS_Store");

        /*check elements in ArrayLst<String> files_path

        for(int i = 0; i < files_path.size(); i++){
            System.out.println(i+" :"+files_path.get(i));
        }

         */
    }

    public void fillCardsIcon(){
        for(int i = 0; i < files_path.size(); i++){
            cards_icon.add(new ImageIcon(files_path.get(i)));
        }
    }

    //generate a hand(two cards)
    public ArrayList<String> hand(ArrayList<String> cards){
        int indexOne = (int)(Math.random()*cards.size());
        int indexTwo = (int)(Math.random()*cards.size());
        while(indexOne == indexTwo){
            indexTwo = (int)(Math.random()*cards.size());
        }

        ArrayList<String> hand = new ArrayList<>();
        hand.add(cards.get(indexOne));
        hand.add(cards.get(indexTwo));

        cards.remove(indexOne);
        cards.remove(indexTwo);

        return hand;
    }

    //皇家同花顺 (Royal Straight Flush)
    public ArrayList<String> RoyalStraightFlush(ArrayList<String> deck){
        int[] num = new int[deck.size()];
        int[] flush = new int[deck.size()];
        for(int i = 0; i < deck.size(); i++) {
            num[i] = Integer.parseInt(deck.get(i)) % 13;
            flush[i] = Integer.parseInt(deck.get(i)) / 13;
        }

        boolean checkNum = Arrays.binarySearch(num,1) != -1
                && Arrays.binarySearch(num,10) != -1
                && Arrays.binarySearch(num,11) != -1
                && Arrays.binarySearch(num,12) != -1
                && Arrays.binarySearch(num,13) != -1;
        boolean checkFlush = (flush[0]==flush[1] && flush[1]==flush[2] && flush[2]==flush[3] && flush[3]==flush[4]);

        ArrayList<String> RSF = new ArrayList<>();
        if(checkNum && checkFlush){
            for(int i = 0; i < 5; i++){
                RSF.add(deck.get(i));
            }
        }else{
            RSF.add("-1");
        }

        return RSF;
    }

    //同花顺（Straight Flush）
    public void StraightFlush(ArrayList<String> deck){
        ArrayList<String> straight = Straight(deck);

    }

    //四条 (Four of a kind)


    //葫芦 (Full house)


    //同花（Flush）


    //顺子（Straight）
    public ArrayList<String> Straight(ArrayList<String> deck){
        int[] num = new int[deck.size()];
        int[] flush = new int[deck.size()];
        for(int i = 0; i < deck.size(); i++) {
            num[i] = Integer.parseInt(deck.get(i)) % 13;
            flush[i] = Integer.parseInt(deck.get(i)) / 13;
        }

        int[] index = new int[]{-1,-1,-1,-1,-1};//index where contains a straight
        for(int i = 0; i < num.length; i++){
            int temp1 = -1;
            int temp2 = -1;
            int temp3 = -1;
            int temp4 = -1;
            for(int j = 0; j < num.length; j++){
                if(num[i] == num[j]-1){
                    temp1 = j;
                }
                if(num[i] == num[j]-2){
                    temp2 = j;
                }
                if(num[i] == num[j]-3){
                    temp3 = j;
                }
                if(num[i] == num[j]-4){
                    temp4 = j;
                }
            }
            if(temp1 != -1 && temp2 != -1 && temp3 != -1 && temp4 != -1){
                index[0] = i;
                index[1] = temp1;
                index[2] = temp2;
                index[3] = temp3;
                index[4] = temp4;
            }
        }

        boolean checkStraight = true;
        for(int element : index){
            if(element == -1){
                checkStraight = false;
            }
        }

        ArrayList<String> straight = new ArrayList<>();
        if(checkStraight){
            for(int i = 0; i < index.length; i++){
                straight.add(deck.get(index[i]));
            }
        }else{
            straight.add("-1");
        }

        return straight;
    }

    //三条（Three of a kind）


    //两对（Two pairs）


    //一对（One pair）


    //高牌 (High card)


}
